#!/bin/bash

if [ "$POSTGRES_HOST" ]; then
	if [ ! "$POSTGRES_PORT" ]; then
		POSTGRES_PORT=5432
	fi
	if [ ! "$POSTGRES_DB" ]; then
		POSTGRES_DB=postgres
	fi
	if [ ! "$POSTGRES_USER" ]; then
		POSTGRES_USER=postgres
	fi
	if [ ! "$POSTGRES_PASSWORD" ]; then
		POSTGRES_PASSWORD=postgres
	fi

	# Use postgresql for hibernate
	sed -i s/hsql/postgresql/g $PENTAHO_SERVER/pentaho-solutions/system/hibernate/hibernate-settings.xml
	sed -i s/localhost:5432/$POSTGRES_HOST:$POSTGRES_PORT/g $PENTAHO_SERVER/pentaho-solutions/system/hibernate/postgresql.hibernate.cfg.xml
	sed -i "s|postgresql://localhost:5432|postgresql://$POSTGRES_HOST:$POSTGRES_PORT|g" $PENTAHO_SERVER/pentaho-solutions/system/simple-jndi/jdbc.properties

	# Use postgresql for tomcat (quartz / hibernate)
	sed -i "s|jdbc:postgresql://localhost:5432/|jdbc:postgresql://$POSTGRES_HOST:$POSTGRES_PORT/|g" $PENTAHO_SERVER/tomcat/webapps/pentaho/META-INF/context.xml
	
	# Use postgresql for jackrabbit
	sed -i "s|jdbc:postgresql://localhost:5432/|jdbc:postgresql://$POSTGRES_HOST:$POSTGRES_PORT/|g" $PENTAHO_SERVER/pentaho-solutions/system/jackrabbit/repository.xml
	
	echo "Checking PostgreSQL connection ..."
	nc -zv $POSTGRES_HOST $POSTGRES_PORT
	if [ "$?" -ne "0" ]; then
		echo "PostgreSQL connection failed."
		exit 0
	fi
	
	CHK_JCR=`echo "$(psql -h $POSTGRES_HOST -U $POSTGRES_USER -d $POSTGRES_DB -l | grep jackrabbit | wc -l)"`
	echo "jcr: $CHK_JCR"
	if [ "$CHK_JCR" -eq "0" ]; then
		psql -h $POSTGRES_HOST -U $POSTGRES_USER -d $POSTGRES_DB -f $PENTAHO_SERVER/data/postgresql/create_jcr_postgresql.sql
	fi

	CHK_HIBERNATE=`echo "$(psql -h $POSTGRES_HOST -U $POSTGRES_USER -d $POSTGRES_DB -l | grep hibernate | wc -l)"`
	echo "hibernate: $CHK_HIBERNATE"
	if [ "$CHK_HIBERNATE" -eq "0" ]; then
		psql -h $POSTGRES_HOST -U $POSTGRES_USER -d $POSTGRES_DB -f $PENTAHO_SERVER/data/postgresql/create_repository_postgresql.sql
	fi

	CHK_QUARTZ=`echo "$(psql -h $POSTGRES_HOST -U $POSTGRES_USER -d $POSTGRES_DB -l | grep quartz | wc -l)"`
	echo "quartz: $CHK_QUARTZ"
	if [ "$CHK_QUARTZ" -eq "0" ]; then
		psql -h $POSTGRES_HOST -U $POSTGRES_USER -d $POSTGRES_DB -f $PENTAHO_SERVER/data/postgresql/create_quartz_postgresql.sql
	fi
fi
