#!/bin/bash

# Setup postgres on init
#sh ${PENTAHO_SERVER}/scripts/setup_postgresql.sh

# Setup keycloak on init
sh ${PENTAHO_SERVER}/scripts/setup_keycloak.sh

# Running pentaho
sh ${PENTAHO_SERVER}/start-pentaho.sh