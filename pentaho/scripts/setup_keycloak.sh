#!/bin/bash

if [ ! "$KEYCLOAK_HOST" ]; then
    KEYCLOAK_HOST="localhost"
fi

if [ ! "$KEYCLOAK_REALM" ]; then
    KEYCLOAK_REALM="pentaho-realm"
fi

if [ ! "$KEYCLOAK_RSA_KEY" ]; then
    KEYCLOAK_RSA_KEY="**keycloak-rsa-key**"
fi

if [ ! "$KEYCLOAK_RSA_CERT" ]; then
    KEYCLOAK_RSA_CERT="**keycloak-rsa-cert**"
fi

if [ ! "$KEYCLOAK_CLIENT" ]; then
    KEYCLOAK_CLIENT="pentaho-client"
fi

if [ ! "$KEYCLOAK_LOGOUT_HOST" ]; then
    KEYCLOAK_LOGOUT_HOST="localhost"
fi

sed -i "s/\*\*keycloak-host\*\*/${KEYCLOAK_HOST}/g" $PENTAHO_SERVER/pentaho-solutions/saml/keycloak-idp.xml
sed -i "s/\*\*keycloak-host\*\*/${KEYCLOAK_HOST}/g" $PENTAHO_SERVER/pentaho-solutions/system/karaf/etc/pentaho.saml.cfg
sed -i "s/\*\*keycloak-realm\*\*/${KEYCLOAK_REALM}/g" $PENTAHO_SERVER/pentaho-solutions/saml/keycloak-idp.xml
sed -i "s/\*\*keycloak-realm\*\*/${KEYCLOAK_REALM}/g" $PENTAHO_SERVER/pentaho-solutions/system/karaf/etc/pentaho.saml.cfg
sed -i "s#\*\*keycloak-rsa-key\*\*#${KEYCLOAK_RSA_KEY}#" $PENTAHO_SERVER/pentaho-solutions/saml/keycloak-idp.xml
sed -i "s#\*\*keycloak-rsa-cert\*\*#${KEYCLOAK_RSA_CERT}#" $PENTAHO_SERVER/pentaho-solutions/saml/keycloak-idp.xml
sed -i "s/\*\*keycloak-client\*\*/${KEYCLOAK_CLIENT}/g" $PENTAHO_SERVER/pentaho-solutions/saml/keycloak-idp.xml
sed -i "s/\*\*keycloak-client\*\*/${KEYCLOAK_CLIENT}/g" $PENTAHO_SERVER/pentaho-solutions/system/karaf/etc/pentaho.saml.cfg
sed -i "s/\*\*keycloak-client\*\*/${KEYCLOAK_CLIENT}/g" $PENTAHO_SERVER/pentaho-solutions/saml/pentaho-sp.xml
sed -i "s/\*\*keycloak-logout-host\*\*/${KEYCLOAK_LOGOUT_HOST}/g" $PENTAHO_SERVER/pentaho-solutions/saml/pentaho-sp.xml
