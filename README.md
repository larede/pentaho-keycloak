# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Prova de conceito no Pentaho com autnenticação e autorização no Keycloak
* Todos os parametro são dinamicos, ou seja, posso ligar-me a qualquer keycloak

### How do I get set up? ###

* docker-compose up -d

	Pentaho:

	http://localhost/pentaho

	Keycloak:

	http://localhost/auth

* Utilizadores para testar

	pentaho / pentaho

	admin-pentaho / pentaho

### NOTAS ###

* #### Ainda comecei a configurar o postgres como deve ser mas depois disisti e ficou a trabalhar com o hsqldb ####
Se tiverem interessado no trabalho que comecei a fazer (setup_postgresql.sh) isto estava quase a funcionar e posso investir mais uns minutos ...

* #### Isto NÂO FUNCIONA com Multitenancy .. para instalações On-Premises tudo Ok !!  ####
Tem que se investir tempo nisto https://help.pentaho.com/Documentation/8.3/Developer_center/Multi-tenancy

* #### Falta validar ainda se o SSO entre "sessões" OpenId Connect e SAML funciona bem !!  ####
Com este compose já é muito fácil validar .. quando eu tiver 5 minutos faço isso!!